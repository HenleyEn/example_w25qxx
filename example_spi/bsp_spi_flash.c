#include "bsp_spi_flash.h"
#include "bsp_spi_bus.h"
#include "hosal_gpio.h"
#include <stdio.h>
#include <string.h>

typedef enum{RESET = 0, SET = !RESET}Reg_Status_t;

#define SPI_FLAHS_CS_PIN        14

#define CS_0()    bl_gpio_output_set(SPI_FLAHS_CS_PIN, 0)
#define CS_1()    bl_gpio_output_set(SPI_FLAHS_CS_PIN, 1)

#define W25X_WRITE_ENABLE                0x06
#define W25X_WRITE_DISABLE               0x04
#define W25X_READ_STATUS_REG             0x05
#define W25X_WRITE_STATUS_REG            0x01
#define W25X_READ_DATA                   0x03
#define W25X_FAST_READ_DATA              0x0B
#define W25X_FAST_READ_DUAL              0x3B
#define W25X_PAGE_PROGRAM                0x02
#define W25X_BLOCK_ERASE                 0xD8
#define W25X_SECTOR_ERASE                0x20
#define W25X_CHIP_ERASE                  0xC7
#define W25X_POWER_DOWN                  0xB9
#define W25X_DEVICE_ID                   0xAB
#define W25X_MANBUFAT_DEVICE_ID          0x90
#define W25X_JEDEC_DEVICE_ID             0x9F

#define WIP_FLAG                        0x01 /* Write In Progress (WIP) flag */

#define Dummy_Byte                      0xFF

#define W25QX_TIMEOUT                   100

flash_info_t flash_dev_info;

#define BLOCK_COUNTS        (flash_dev_info.total_size / flash_dev_info.block_size)
#define SECTOR_COUNTS       (flash_dev_info.block_size / flash_dev_info.sector_size)

static uint8_t p_spi_buffer[SPI_BUFFER_SIZE];   //flash中私有数据，用于存储读出的数据，用于比对需要发送的数据

/**
 * @brief 设置片选cs脚
 *
 * @param level 
 */
static void flash_set_cs(uint8_t level)
{
    if (level == 0)
    {
        bsp_spi_bus_enter();	
        bsp_spi_bus_init_param(2000000, 0, NULL, NULL);
        CS_0();
    }
    else
    {		
        CS_1();	
        bsp_spi_bus_exit();		
    }

}

/**
 * @brief flash 写使能打开
 * 
 */
static void flash_write_enable(void)
{
    flash_set_cs(0);

    g_buffer_len = 0;
    g_tx_buffer[g_buffer_len ++] = W25X_WRITE_ENABLE;
    bsp_spi_bus_transfer();
    
    flash_set_cs(1);
}

/**
 * @brief flash写使能关闭
 * 
 */
static void flash_write_disable(void)
{
    flash_set_cs(0);
    g_buffer_len = 0;
    g_tx_buffer[g_buffer_len ++] = W25X_WRITE_DISABLE;
    bsp_spi_bus_transfer();
    
    flash_set_cs(1);

}

/**
 * @brief 等待flash操作完成
 * 
 */
static void flash_wait_for_write_end(void)
{
    flash_set_cs(0);
    g_tx_buffer[0] = (W25X_READ_STATUS_REG);
    g_buffer_len = 2;
    bsp_spi_bus_transfer();
    flash_set_cs(1);

    while(1)
    {
        flash_set_cs(0);
        g_tx_buffer[0] = (W25X_READ_STATUS_REG);
        g_tx_buffer[1] = 0;
        g_buffer_len = 2;
        bsp_spi_bus_transfer();	
        flash_set_cs(1);
		
		if ((g_rx_buffer[1] & WIP_FLAG) != SET)
		{
			break;
		}
    }
}

/**
 * @brief 判断是否为扇区首地址
 * 
 * @param addr 输入地址
 * @return uint8_t 
 *          - 1 : yes
 *          - 0 : no
 */
static uint8_t flash_get_sector_first_addr_flag(uint32_t addr)
{
    int i;
    uint8_t ret = 0;
    uint32_t temp = 0;

    for(i = 0; i < (SECTOR_COUNTS * BLOCK_COUNTS); i++)
    {
        if(addr == temp)
        {
            ret = 1;
            return ret;
        }
        temp += flash_dev_info.sector_size;

    }

    return ret;
}


/**
 * @brief 判断是否为块首地址
 * 
 * @param addr 输入地址
 * @return uint8_t 
 *          - 1 : yes
 *          - 0 : no
 */
static uint8_t flash_get_block_first_addr_flag(uint32_t addr)
{
    int i;
    uint8_t ret = 0;
    uint32_t temp = 0;

    for(i = 0; i < BLOCK_COUNTS; i++)
    {
        if(addr == temp)
        {
            ret = 1;
            return ret;
        }
        temp += flash_dev_info.block_size;
    }
    
    return ret;
}

/**
 * @brief 判断是否需要擦除，减少flash的额擦除次数，延长flash的使用寿命
 * 
 * @param old_buf  旧数据
 * @param new_buf  要写入的数据
 * @param size 大小
 * @return uint8_t 
 *          - 1 ： 需要擦除
 *          - 0 ：不需要擦除
 */
static uint8_t flash_get_erase_flag(uint8_t * old_buf, uint8_t *new_buf, uint16_t size)
{
    uint32_t i;
    uint32_t old_data;

    /* 
        flash 不支持覆盖写入
        flash 只能将1 -> 0，不能将 0 -> 1
        根本原因其实是flash芯片的写操作只能将1变为0，而不能将0变为1，
        擦除之后，flash中是全1的状态，若想将0变为1则只能通过擦除操作。
    */

    /*
	算法第1步：old 求反, new 不变
	      old    new
		  1101   0101
	~     1111
		= 0010   0101

	算法第2步: old 求反的结果与 new 位与
		  0010   old
	&	  0101   new
		 =0000

	算法第3步: 结果为0,则表示无需擦除. 否则表示需要擦除
	*/
    for (i = 0; i < size; i++)
    {
        old_data = *old_buf++;
        old_data = ~old_data;

        if ((old_data & (*new_buf++)) != 0)
        {
            return 1;
        }
    }
    return 0;
}

/**
 * @brief 扇区中的数据与即将写入的数据进行比较
 * 
 * @param src_buf 即将写入的数据
 * @param addr 写入地址
 * @param size 写入大小
 * @return uint8_t 
 *          - 1 ：存在不同
 *          - 0 ：相同
 */
static uint8_t flash_compare_data(uint8_t *src_buf, uint32_t addr, uint32_t size)
{
    uint32_t i, j;
    uint32_t remain;

    if((addr + size) > flash_dev_info.total_size)
    {
        printf("addr or size is error in flash compare function\r\n");
        return 1;
    }

    if(size == 0)
    {
        return 0;
    }

    // flash_write_enable();

    flash_set_cs(0);
    g_buffer_len = 0;

    g_tx_buffer[g_buffer_len ++] = W25X_READ_DATA;
    g_tx_buffer[g_buffer_len ++] = ((addr & 0xFF0000) >> 16);
    g_tx_buffer[g_buffer_len ++] = ((addr & 0xFF00) >> 8);
    g_tx_buffer[g_buffer_len ++] = (addr & 0xFF);

    bsp_spi_bus_transfer();

    for(i = 0; i < (size / SPI_BUFFER_SIZE); i++)
    {
        g_buffer_len = SPI_BUFFER_SIZE;
        bsp_spi_bus_transfer();

        for(j = 0; j < SPI_BUFFER_SIZE; j++)
        {
            if (g_rx_buffer[j] != *src_buf++)
			{
				goto NOTEQ;		/* 不相等 */
			}

        }
    }

    remain = size % SPI_BUFFER_SIZE;

    if(remain > 0)
    {
        g_buffer_len = remain;
        bsp_spi_bus_transfer();

        for(j = 0; j < remain; j++)
        {
            if (g_rx_buffer[j] != *src_buf++)
			{
				goto NOTEQ;		/* 不相等 */
			}

        }
    }

    flash_set_cs(1);
    return 0; // OK

NOTEQ:
    flash_set_cs(1);
    return 1; // different
}

/**
 * @brief 自动写入扇区函数，实现
 * 
 * @param src_buf 
 * @param write_addr 
 * @param size 
 * @return uint8_t 
 */
static uint8_t flash_auto_write_sector(uint8_t *src_buf, uint32_t write_addr, uint16_t size)
{
    uint32_t i, j;
    uint32_t first_addr;
    uint8_t erase_flag = 0;
    uint8_t ret = 0;

    // no need write
    if(size == 0)
    {
        return 1;
    }

    if(write_addr >= flash_dev_info.total_size)
    {
        printf("write addr is wrong\r\n");
        return 0;
    }
    
    if(size > flash_dev_info.sector_size)
    {
        printf("write size is overflow\r\n");
        return 0;
    }

    /* 读取flash中的数据，与需要写入的数据进行比对，如果没有变化，就不写进flash中 */
    flash_buffer_read(p_spi_buffer, write_addr, size);
    if(memcmp(p_spi_buffer, src_buf, size) == 0)
    {
        return 1;
    }

    /* 判断是否需要擦除扇区 */
    if(flash_get_erase_flag(p_spi_buffer, src_buf, size))
    {
        erase_flag = 1;
    }

    first_addr = write_addr & (~(flash_dev_info.sector_size - 1));
    printf("first addr = 0x%X, write addr = 0x%X\r\n", first_addr, write_addr);

    /* 整片扇区都替换新数据 */
    if(size == flash_dev_info.sector_size)
    {
        for(i = 0; i < flash_dev_info.sector_size; i++)
        {
            p_spi_buffer[i] = src_buf[i];
        }
    }
    else
    {
        /* 找到第一个变化的数据，修改之后的所有数据 */
        /* 先读出扇区的数据 */
        flash_buffer_read(p_spi_buffer, first_addr, flash_dev_info.sector_size);

        /* 放入新数据 */
        i = write_addr & (flash_dev_info.sector_size - 1);
        memcpy(&p_spi_buffer[i], src_buf, size);
    }

    /* 开始写入数据，并校验，重写机制，重写3次 */
    uint8_t rewrite_count = 0;
    for(i = 0; i < 3 ; i++)
    {
        /* 是否需要先擦除一个扇区 */
        if(erase_flag == 1)
        {
            flash_erase_sector(first_addr);
        }
        /* 对一个页进行编程 */
        flash_page_write(p_spi_buffer, first_addr, flash_dev_info.sector_size);

        if(flash_compare_data(src_buf, write_addr, size) == 0)
        {
            ret = 1;
            break;
        }
        else
        {
            rewrite_count++;
            printf("write error and try to rewrite count:%d\r\n", rewrite_count);
            for(j = 0; j < 10000; j++);
        }
    }
    return ret;
}

void bsp_init_flash(void)
{
    /* set cs pin and spi */
    bl_gpio_enable_output(SPI_FLAHS_CS_PIN, 0, 0);
    bl_gpio_output_set(SPI_FLAHS_CS_PIN, 1);

    bsp_spi_bus_init();
    flash_read_info();
    printf("flash init OK\r\n");
}

uint32_t flash_read_id(void)
{
    uint32_t device_id;

    flash_set_cs(0);
    g_buffer_len = 0;
    g_tx_buffer[0] = W25X_JEDEC_DEVICE_ID;
    g_buffer_len = 4;
    bsp_spi_bus_transfer();
    flash_set_cs(1);

    device_id = (g_rx_buffer[1] << 16) | (g_rx_buffer[2] << 8) | g_rx_buffer[3];
    return device_id;
}

void flash_erase_chip(void)
{
    flash_write_enable();

    flash_set_cs(0);
    g_buffer_len = 0;
    g_tx_buffer[g_buffer_len ++] = W25X_CHIP_ERASE;
    bsp_spi_bus_transfer();
    flash_set_cs(1);

    flash_wait_for_write_end();

    printf("erase chip done\r\n");

}

uint8_t flash_erase_block(uint32_t block_addr)
{

    if(flash_get_block_first_addr_flag(block_addr) == 0)
    {   
        printf("erase block error, first block address is wrong\r\n");
        return 0;
    }
    else
    {
        flash_write_enable();

        flash_set_cs(0);
        g_buffer_len = 0;
        g_tx_buffer[g_buffer_len ++] = W25X_BLOCK_ERASE;

        g_tx_buffer[g_buffer_len ++] = ((block_addr & 0xFF0000) >> 16);
        g_tx_buffer[g_buffer_len ++] = ((block_addr & 0xFF00) >> 8);
        g_tx_buffer[g_buffer_len ++] = (block_addr & 0xFF);

        bsp_spi_bus_transfer();
        flash_set_cs(1);

        flash_wait_for_write_end();
        
        printf("erase block done\r\n");
    }

    return 1;
}

uint8_t flash_erase_sector(uint32_t sector_addr)
{
    if(flash_get_sector_first_addr_flag(sector_addr) == 0)
    {   
        printf("erase sector error, first sector address is wrong\r\n");
        return 0;
    }
    else
    {
        flash_write_enable();

        flash_set_cs(0);
        g_buffer_len = 0;
        g_tx_buffer[g_buffer_len ++] = W25X_SECTOR_ERASE;

        g_tx_buffer[g_buffer_len ++] = ((sector_addr & 0xFF0000) >> 16);
        g_tx_buffer[g_buffer_len ++] = ((sector_addr & 0xFF00) >> 8);
        g_tx_buffer[g_buffer_len ++] = (sector_addr & 0xFF);

        bsp_spi_bus_transfer();

        flash_set_cs(1);
        flash_wait_for_write_end();

        printf("erase sector done\r\n");
    }
    return 1;

}

void flash_page_write(uint8_t * pbuf, uint32_t write_addr, uint16_t size)
{
    int i, j;

    if((size % 256) != 0)
    {
        printf("cant write page\r\n");
        return;
    }

    for(i = 0; i < (size / 256); i++)
    {
        flash_write_enable();

        flash_set_cs(0);
        g_buffer_len = 0;

        g_tx_buffer[g_buffer_len ++] = W25X_PAGE_PROGRAM;
        g_tx_buffer[g_buffer_len ++] = ((write_addr & 0xFF0000) >> 16);
        g_tx_buffer[g_buffer_len ++] = ((write_addr & 0xFF00) >> 8);
        g_tx_buffer[g_buffer_len ++] = (write_addr & 0xFF);

        for(j = 0; j < 256; j++)
        {
            g_tx_buffer[g_buffer_len ++] = (*pbuf++);
        }
        bsp_spi_bus_transfer();
        flash_set_cs(1);
        
        flash_wait_for_write_end();

        write_addr += 256;
    }
    flash_write_disable();
    flash_wait_for_write_end();
}

uint8_t flash_buffer_write(uint8_t * pbuf, uint32_t write_addr, uint16_t size)
{
    uint32_t num_of_sector = 0;
    uint32_t num_of_remain = 0;
    uint32_t addr;
    uint32_t count;

    addr = write_addr % flash_dev_info.sector_size;     //获取在扇区中的地址
    count = flash_dev_info.sector_size - addr;          //扇区中剩余容量
    num_of_sector =  size / flash_dev_info.sector_size; //需要写入扇区数量
    num_of_remain = size % flash_dev_info.sector_size;  //写完扇区后，需要写入的剩余数量

    /* 首区是扇区的首地址 */
    if(addr == 0)
    {
        if(num_of_sector == 0)
        {
            if(flash_auto_write_sector(pbuf, write_addr, size) == 0)
            {
                printf("write buffer error\r\n");
                return 0;
            }
        }
        else
        {
            /* 先填充扇区数据 */
            while(num_of_sector--)
            {
                if(flash_auto_write_sector(pbuf, write_addr, flash_dev_info.sector_size) == 0)
                {
                    printf("write buffer error\r\n");
                    return 0;
                }

                write_addr += flash_dev_info.sector_size;
                pbuf += flash_dev_info.sector_size;
            }

            /* 再填充多出来的数据 */
            if(flash_auto_write_sector(pbuf, write_addr, num_of_remain) == 0)
            {
                printf("write buffer error\r\n");
                return 0;
            }

        }
    }
    else    /* 首区不是扇区首地址 */
    {
        /* 写入数据大小小于一个扇区 */
        if(num_of_sector == 0)
        {   
            /* 大小比需要写入的当前扇区还大，涉及两个扇区 */
            if(num_of_remain > count)
            {
                uint8_t temp;

                /* 记录需要在下一个扇区中写入数据的大小 */
                temp = num_of_remain - count;

                if(flash_auto_write_sector(pbuf, write_addr, count) == 0)
                {
                    printf("write buffer error\r\n");
                    return 0;
                }

                write_addr += count;
                pbuf += count;

                if(flash_auto_write_sector(pbuf, write_addr, temp) == 0)
                {
                    printf("write buffer error\r\n");
                    return 0;
                }
            }
            else    /* 写入数据大小能写进当前扇区内 */
            {
                if(flash_auto_write_sector(pbuf, write_addr, size) == 0)
                {
                    printf("write buffer error\r\n");
                    return 0;
                }
            }
        }
        else    /* 写入数据大于一个扇区 */
        {
            size -= count;
            num_of_sector =  size / flash_dev_info.sector_size;
            num_of_remain = size % flash_dev_info.sector_size;

            /* 先填充当前扇区 */
            if(flash_auto_write_sector(pbuf, write_addr, count) == 0)
            {
                printf("write buffer error\r\n");
                return 0;
            }

            write_addr += count;
            pbuf += count;

            /* 再填其他扇区数据 */
            while(num_of_sector--)
            {
                if(flash_auto_write_sector(pbuf, write_addr, flash_dev_info.sector_size) == 0)
                {
                    printf("write buffer error\r\n");
                    return 0;
                }

                write_addr += flash_dev_info.sector_size;
                pbuf += flash_dev_info.sector_size;
            }

            /* 最后填充多出来的数据 */
            if(flash_auto_write_sector(pbuf, write_addr, num_of_remain) == 0)
            {
                printf("write buffer error\r\n");
                return 0;
            }        
        }
    }
    return 1; /* OK */
}

void flash_buffer_read(uint8_t * pbuf, uint32_t read_addr, uint32_t size)
{
    uint16_t rem;
    uint16_t i;
    
    if((size == 0) || (read_addr + size) > flash_dev_info.total_size)
    {
        printf("size = %d, (read_addr + size) = %d\r\n", size, (read_addr + size));
        printf("flash read buffer error\r\n");
        return;
    }

    flash_write_enable();

    flash_set_cs(0);
    g_buffer_len = 0;

    g_tx_buffer[g_buffer_len ++] = W25X_READ_DATA;
    g_tx_buffer[g_buffer_len ++] = ((read_addr & 0xFF0000) >> 16);
    g_tx_buffer[g_buffer_len ++] = ((read_addr & 0xFF00) >> 8);
    g_tx_buffer[g_buffer_len ++] = (read_addr & 0xFF);

    bsp_spi_bus_transfer();     //这里先让flash进入读取模式，避免在读取的时候遗漏前4个bytes

    for(i = 0; i < (size / SPI_BUFFER_SIZE); i++)
    {
        g_buffer_len = SPI_BUFFER_SIZE;
        bsp_spi_bus_transfer();	//开始正式读取数据

        memcpy(pbuf, g_rx_buffer, SPI_BUFFER_SIZE);
        pbuf += SPI_BUFFER_SIZE;
    }

    rem = size % SPI_BUFFER_SIZE;

    if(rem > 0)
    {
        g_buffer_len = rem;
        bsp_spi_bus_transfer();
        memcpy(pbuf, g_rx_buffer, rem);
    }
    flash_set_cs(1);
    flash_write_disable();
    flash_wait_for_write_end();
}

void flash_read_info(void)
{
    uint32_t id = flash_read_id();
    
    flash_dev_info.device_id = id;

    switch (id)
    {
    case W25Q64_ID:
        strcpy(flash_dev_info.device_name, "W25Q64");
        flash_dev_info.total_size = 8 * 1024 * 1024;         /* 总容量 = 8M */
        flash_dev_info.block_size = 16 * 4 * 1024;
        flash_dev_info.sector_size = 4 * 1024; /* 页面大小 = 4K */
        break;
    default:
        strcpy(flash_dev_info.device_name, "Unknow Flash");
        flash_dev_info.total_size = 2;
        flash_dev_info.sector_size = 4 * 1024;
        break;
    }
    printf("Chip Name:%s, Chip ID:0x%08X\r\n", flash_dev_info.device_name, flash_dev_info.device_id);
    printf("Chip total size:%d M, Chip Sector Size:%d\r\n", flash_dev_info.total_size / (1024 * 1024), flash_dev_info.sector_size);

}
