#include "bsp_spi_bus.h"
#include "hosal_spi.h"
#include <stdio.h>

#define SPI_SCLK          3
#define SPI_MOSI          17  //D0
#define SPI_MISO          12  //D1

static hosal_spi_dev_t spi0 = {
    .config = {
        .dma_enable = 0,
        .freq = 2000000,
        .mode = HOSAL_SPI_MODE_MASTER,
        .pin_clk = SPI_SCLK,
        .pin_miso = SPI_MOSI,
        .pin_mosi = SPI_MISO,
        .polar_phase = 0,
    },
    .cb = NULL,
    .port = 0,
};

uint8_t g_spi_busy_flag; /* SPI忙状态，0表示不忙，1表示忙 */
uint16_t g_buffer_len;

uint8_t g_tx_buffer[SPI_BUFFER_SIZE];
uint8_t g_rx_buffer[SPI_BUFFER_SIZE];

void bsp_spi_bus_init(void)
{
    g_spi_busy_flag = 0;
    bsp_spi_bus_init_param(2000000, 0, NULL, NULL);
    hosal_spi_init(&spi0);
}

/**
 * @brief send and recivce
 * 
 */
void bsp_spi_bus_transfer(void)
{
    if (g_buffer_len > SPI_BUFFER_SIZE)
    {
        printf("buffer size overflow\r\n");
        return;
    }
    if (hosal_spi_send_recv(&spi0, (uint8_t *)g_tx_buffer, (uint8_t *)g_rx_buffer, g_buffer_len, 1000000) != 0)
    {
        printf("spi transfer error\r\n");
    }
}

void bsp_spi_bus_init_param(uint32_t freq, uint8_t polar_phase, hosal_spi_irq_t cb, void *p_arg)
{
    if (spi0.config.freq == freq && spi0.config.polar_phase == polar_phase && spi0.cb == cb)
    {
        return;
    }

    spi0.config.freq = freq;
    spi0.config.polar_phase = polar_phase;

    hosal_spi_irq_callback_set(&spi0, cb, p_arg);

    if (hosal_spi_finalize(&spi0) != 0)
    {
        printf("deinit spi error\r\n");
    }

    if (hosal_spi_init(&spi0) != 0)
    {
        printf("init spi error\r\n");
    }
}

void bsp_spi_bus_enter(void)
{
    g_spi_busy_flag = 1;
}
void bsp_spi_bus_exit(void)
{
    g_spi_busy_flag = 0;
}

uint8_t bsp_spi_bus_get_busy_flag(void)
{
    return g_spi_busy_flag;
}
