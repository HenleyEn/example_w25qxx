#include "demo_spi_flash.h"
#include <string.h>
#include <stdio.h>
#include <hosal_spi.h>
#include <bl_gpio.h>
#include "bsp_spi_flash.h"

// #define ENABLE_1K_DEMO       1
#define ENABLE_WRITE_LOG_DEMO   1

#define ARRAY_SIZE(x)       (sizeof(x) / sizeof(*x))


#if (ENABLE_1K_DEMO == 1)

#define TEST_SIZE   1024
#define TEST_ADDR   0x011301

static uint8_t buf[TEST_SIZE] = {0};

static void write_1K(uint8_t data)
{
    uint32_t i;

    for(i = 0; i < TEST_SIZE; i++)
    {
        buf[i] = data;
    }

    if(flash_buffer_write(buf, TEST_ADDR, TEST_SIZE) == 0)
    {
        printf("write 1K Flash error\r\n");
        return;
    }

    printf("write Flash 1K end, write addr:0x%08X\r\n", TEST_ADDR);
}

static void read_1K(void)
{
    uint32_t i;

	flash_buffer_read(buf, TEST_ADDR,  1024);		/* 读数据 */
	printf("addr: 0x%08X, data size = 1024\r\n", TEST_ADDR);

	/* 打印数据 */
	for (i = 0; i < 1024; i++)
	{
		printf(" %02X", buf[i]);

		if ((i & 31) == 31)
		{
			printf("\r\n");	/* 每行显示16字节数据 */
		}
		else if ((i & 31) == 15)
		{
			printf(" - ");
		}
	}
}

static void read_1K_demo(void)
{
    uint8_t ret;

    ret = flash_erase_sector(0x011000);

    if(ret == 1)
    {
        printf("read 1K before write:\r\n");
        read_1K();

        printf("writing data\r\n");
        write_1K(0x55);

        printf("read 1K after write:\r\n");
        read_1K();

    }
    else
    {
        printf("demo is wrong\r\n");
    }
}

#elif (ENABLE_WRITE_LOG_DEMO == 1)

#define TEST_ADDR   0x012101
#define TEST_FIRST_SECTOR_ADDR  0x012000
static uint8_t log_tx_buf[64] = {"2024-9-16 ERROR : W15\r\n2024-9-16 ERROR : W16\r\n"};
static uint8_t log_rx_buf[64] = {0};

static void log_demo(void)
{
    uint8_t ret;
    int i;
    ret = flash_erase_sector(TEST_FIRST_SECTOR_ADDR);
    
    if(ret == 1)
    {
        printf("read before write:\r\n");
        flash_buffer_read(log_rx_buf, TEST_ADDR, ARRAY_SIZE(log_rx_buf));

        for (i = 0; i < ARRAY_SIZE(log_rx_buf); i++)
        {
            printf(" %02X", log_rx_buf[i]);

            if ((i & 31) == 31)
            {
                printf("\r\n");	/* 每行显示16字节数据 */
            }
            else if ((i & 31) == 15)
            {
                printf(" - ");
            }
        }

        printf("writing data\r\n");
        flash_buffer_write(log_tx_buf, TEST_ADDR, ARRAY_SIZE(log_tx_buf));

        printf("read after write:\r\n");
        flash_buffer_read(log_rx_buf, TEST_ADDR, ARRAY_SIZE(log_rx_buf));
        printf("%s", log_rx_buf);

    }
    else
    {
        printf("log demo is wrong\r\n");
    }

}
#endif

void demo_flash(void)
{
    bsp_init_flash();

#if (ENABLE_1K_DEMO == 1)
    read_1K_demo();
#elif (ENABLE_WRITE_LOG_DEMO == 1)
    log_demo();
#endif
}

