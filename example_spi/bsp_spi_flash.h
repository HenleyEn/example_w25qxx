#ifndef BSP_SPI_FLASH_H
#define BSP_SPI_FLASH_H

#include <hosal_spi.h>
#include <bl_gpio.h>

enum
{
    W25Q64_ID = 0xEF4017U,
};

typedef struct 
{
    char device_name[20];
    uint32_t device_id;
    uint32_t total_size;
    uint32_t block_size;
    uint32_t sector_size;
}flash_info_t;

/**
 * @brief flash init
 * 
 */
void bsp_init_flash(void);

/**
 * @brief read flash JEDEC ID
 * 
 * @return uint32_t - JEDEC ID
 */
uint32_t flash_read_id(void);

/**
 * @brief erase chip
 * 
 */
void flash_erase_chip(void);

/**
 * @brief erase block
 * 
 * @param block_addr - the first block address
 * @return uint8_t 
 *      - 1:erase block done
 *      - 0:erase block error
 */
uint8_t flash_erase_block(uint32_t block_addr);

/**
 * @brief erase sector
 * 
 * @param sector_addr - the first sector address
 * @return uint8_t 
 *      - 1: erase sector done
 *      - 0: erase sector error
 */
uint8_t flash_erase_sector(uint32_t sector_addr);

/**
 * @brief write data on page
 * 
 * @param pbuf prepare to write data
 * @param write_addr write address
 * @param size buffer size must (size % 256 == 0)
 */
void flash_page_write(uint8_t * pbuf, uint32_t write_addr, uint16_t size);

/**
 * @brief write buffer to flash anywhere you want
 * 
 * @param pbuf prepare to write data
 * @param write_addr write address
 * @param size buffer size
 * @return uint8_t 
 *      - 1: write done
 *      - 0: write error
 */
uint8_t flash_buffer_write(uint8_t * pbuf, uint32_t write_addr, uint16_t size);

/**
 * @brief read flash data
 * 
 * @param pbuf save data when read flash data
 * @param read_addr read address
 * @param size read size
 */
void flash_buffer_read(uint8_t * pbuf, uint32_t read_addr, uint32_t size);

/**
 * @brief set flash device information
 * 
 */
void flash_read_info(void);

#endif /* endif BSP_SPI_FLASH_H */

