#ifndef BSP_SPI_BUS_H
#define BSP_SPI_BUS_H

#include "hosal_gpio.h"
#include "hosal_spi.h"

#define SPI_BUFFER_SIZE     (4*1024)

extern uint8_t g_spi_busy_flag;
extern uint16_t g_buffer_len;

extern uint8_t g_tx_buffer[SPI_BUFFER_SIZE];
extern uint8_t g_rx_buffer[SPI_BUFFER_SIZE];


void bsp_spi_bus_init(void); 
void bsp_spi_bus_transfer(void);
void bsp_spi_bus_init_param(uint32_t freq, uint8_t polar_phase, hosal_spi_irq_t cb, void *p_arg);

void bsp_spi_bus_enter(void);
void bsp_spi_bus_exit(void);
uint8_t bsp_spi_bus_busy(void);

#endif /* endif BSP_SPI_BUS_H */



