# Example: Ai-WB2 Series SoC Module W25Q64 Driver

## Hardware Setup and Wiring

| Ai-WB2 Series SoC Module Pinout | W25Q64 Pinout |
|---|---|
| IO3(SPI_SCLK) | SCLK |
| IO4(SPI_FLAHS_CS_PIN) | CS |
| IO17(SPI_MOSI) | D0 |
| IO12(SPI_MISO) | D0 |
| 3V3 | VCC |
| GND | GND |

## Build and Flash

```shell
make -j
make flash
```

## Run

![img](img/image-20240909190551520.png)

![img](img/image-20240909190638872.png)
